<?php

function db_connect(){
    $host = "172.18.0.3";
    $user = "rush";
    $passwd = "123";
    $dbname = "rushdb";

    $conn = mysqli_connect($host, $user, $passwd, $dbname);

    if(!$conn){
        echo "Erreur : Impossible de se connecter à MySQL." . PHP_EOL;
        echo "Errno de débogage : " . mysqli_connect_errno() . PHP_EOL;
        echo "Erreur de débogage : " . mysqli_connect_error() . PHP_EOL;
        exit();
    }

    return $conn;
}
